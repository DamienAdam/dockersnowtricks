# Container for Snowtricks project, 

OpenClassrooms Backend/Symfony Developer path, project #6

- install docker
- clone on computer
- cd to folder
- (OPTIONAL clone Snowtricks project into container folder) https://gitlab.com/DamienAdam/snowtricks.git
- docker-compose build
- docker-compose up
